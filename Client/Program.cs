﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using Client.Business;
using Common;
using NLog;

namespace Client
{
    internal static class Program
    {
        private const string Endpoint = "Client";
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private static void Main(string[] args)
        {
            Log.Debug("Application started");

            Log.Debug("Initializing client");
            var config = new Dictionary<string, string>();
            config["port"] = "0";
            config["name"] = Endpoint;
            var channel = new TcpChannel(
                config,
                new BinaryClientFormatterSinkProvider(),
                new BinaryServerFormatterSinkProvider()
                );
            ChannelServices.RegisterChannel(channel, false);

            var clientSvc = new ClientSvcImpl(args[3], args[5]);

            RemotingServices.Marshal(clientSvc, Endpoint);

            // Get client port
            var channelData = (ChannelDataStore) channel.ChannelData;
            var clientUri = String.Format("{0}/{1}", channelData.ChannelUris[0], Endpoint);

            Log.Debug("Client service started at: {0}", clientUri);
            Log.Debug("Job tracker address: {0}", args[1]);
            // Reading the DLL
            var code = File.ReadAllBytes(args[0]);

            // Create the worker service
            var workerSvc = (IWorkerSvc) Activator.GetObject(
                typeof (IWorkerSvc), args[1]
                );

            var fileInfo = new FileInfo(args[3]);
            // Send the mapper to server
            try
            {
                var sendResult = workerSvc.SendMapper(code, args[2], clientUri, fileInfo.Length,
                    Convert.ToInt32(args[4]));
                Log.Debug("Server return: {0}", sendResult);
                if (sendResult)
                {
                    Console.WriteLine("Mapper submit successfully");
                }
                else
                {
                    Console.WriteLine("Unable to submit mapper");
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception executing job", e);
            }

            Console.WriteLine("Waiting for result...");
            Console.ReadLine();
        }
    }
}