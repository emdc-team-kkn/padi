﻿using System;
using System.Collections.Generic;
using System.IO;
using Common;
using NLog;

namespace Client.Business
{
    internal class ClientSvcImpl : MarshalByRefObject, IClientSvc
    {
        private const int MaxBuffer = 4096*1024;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly string _inputPath;
        private readonly string _outputDir;
        private readonly HashSet<int> _pendingOutput;
        private readonly HashSet<int> _writtenOutput;

        public ClientSvcImpl(string inputPath, string outputDir)
        {
            _inputPath = inputPath;
            _outputDir = outputDir;
            _pendingOutput = new HashSet<int>();
            _writtenOutput = new HashSet<int>();
        }

        public byte[] ReadInput(long offset, int maxLength, out int readLength)
        {
            var bufferLength = Math.Min(maxLength, MaxBuffer);
            var buffer = new byte[bufferLength];
            using (var reader = new FileStream(_inputPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                reader.Seek(offset, SeekOrigin.Begin);
                readLength = reader.Read(buffer, 0, maxLength);
            }
            return buffer;
        }

        public void WriteOutput(int splitId, KeyValuePair<string, IList<string>>[] result)
        {
            if (!Directory.Exists(_outputDir))
            {
                Directory.CreateDirectory(_outputDir);
            }
            Log.Debug("Receiving result: Split: {0}, Records: {1}", splitId, result.Length);
            var outputFileName = String.Format("{0}.out", splitId);
            var outputPath = Path.Combine(_outputDir, outputFileName);
            Log.Debug("Writing result to: {0}", outputPath);
            using (var writer = new StreamWriter(outputPath))
            {
                writer.WriteLine("Key,Value");
                foreach (var item in result)
                {
                    var key = item.Key;
                    foreach (var s in item.Value)
                    {
                        writer.WriteLine("{0},{1}", key, s);
                    }
                }
            }
            Log.Debug("Write complete for split: {0}", splitId);
        }

        public void ExecutionComplete(bool success)
        {
            if (success)
            {
                Console.WriteLine("Mapper run complete. Press Enter to exit...");
            }
            else
            {
                Console.WriteLine("Unable to complete mapper run. Please try again later. Press Enter to exit...");
            }
        }

        /// <summary>
        ///     Override to provide infinite lease time
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}