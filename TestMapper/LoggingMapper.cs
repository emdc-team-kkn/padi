﻿using System;
using System.Collections.Generic;
using Common;

namespace TestMapper
{
    public class LoggingMapper : IMapper
    {
        public IList<KeyValuePair<string, string>> Map(string fileLine)
        {
            Console.WriteLine("Processing line: {0}", fileLine);
            return null;
        }
    }
}