﻿using System.Collections.Generic;
using System.Linq;
using Common;

namespace TestMapper
{
    internal class WordLengthMapper : IMapper
    {
        public IList<KeyValuePair<string, string>> Map(string fileLine)
        {
            var map = new Dictionary<int, int>();
            var line = fileLine.Trim();
            if (line.Length > 0)
            {
                var tokens = line.Split();
                foreach (var token in tokens)
                {
                    if (!map.ContainsKey(token.Length))
                    {
                        map[token.Length] = 0;
                    }
                    map[token.Length] += 1;
                }
            }
            return map.Select(
                item => new KeyValuePair<string, string>(item.Key.ToString(), item.Value.ToString())
                ).ToList();
        }
    }
}