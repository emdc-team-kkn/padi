﻿using System.Collections.Generic;
using Common;

namespace TestMapper
{
    internal class WordCountMapper : IMapper
    {
        public IList<KeyValuePair<string, string>> Map(string fileLine)
        {
            var ret = new List<KeyValuePair<string, string>>();
            var line = fileLine.Trim();
            int wordCount;
            if (line.Length == 0)
            {
                wordCount = 0;
            }
            else
            {
                wordCount = line.Split().Length;
            }
            ret.Add(new KeyValuePair<string, string>("*", wordCount.ToString()));
            return ret;
        }
    }
}