﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Worker.DataStructure
{
    [Serializable]
    class Decree
    {
        public readonly string Client;
        public readonly string Coordinator;

        public Decree(string client, string coordinator)
        {
            Client = client;
            Coordinator = coordinator;
        }

        public override string ToString()
        {
            return string.Format("Client: {0}, NewCoordinator: {1}", Client, Coordinator);
        }
    }
}
