﻿using System;

namespace Worker.DataStructure
{
    [Serializable]
    public class Task
    {
        public readonly string ClassName;
        public readonly string ClientUri;
        public readonly byte[] Code;
        public readonly long InputSize;
        public readonly int SplitCount;

        public Task(string className, string clientUri, byte[] code, long inputSize, int splitCount)
        {
            ClassName = className;
            ClientUri = clientUri;
            Code = code;
            InputSize = inputSize;
            SplitCount = splitCount;
        }

        public override string ToString()
        {
            return string.Format("ClassName: {0}, ClientUri: {1}, InputSize: {2}, SplitCount: {3}",
                ClassName, ClientUri, InputSize, SplitCount);
        }
    }
}