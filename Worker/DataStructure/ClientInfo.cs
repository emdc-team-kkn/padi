﻿using System;
using System.Collections.Generic;
using Worker.Business;

namespace Worker.DataStructure
{
    [Serializable]
    internal class ClientInfo
    {
        public readonly IDictionary<int, IDictionary<string, IList<string>>> ResultCache;
        public string CoordinatorUri;
        public JobScheduler Scheduler;
        public Task Task;
        public readonly HashSet<int> WrittenJob;

        public ClientInfo()
        {
            ResultCache = new Dictionary<int, IDictionary<string, IList<string>>>();
            WrittenJob = new HashSet<int>();
        }

        public void StartScheduler()
        {
            Scheduler.Start();
        }
    }
}