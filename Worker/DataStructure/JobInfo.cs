﻿using System;

namespace Worker.DataStructure
{
    [Serializable]
    internal class JobInfo
    {
        public Job Job;
        public DateTime StartTime;
        public bool Success;
        public PeerInfo Worker;

        public override string ToString()
        {
            return string.Format("Job: {0}, Success: {1}, StartTime: {2}, Worker: {3}",
                Job, Success, StartTime, Worker);
        }
    }
}