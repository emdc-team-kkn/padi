﻿using System;
using System.Threading;
using Worker.Business;

namespace Worker.DataStructure
{
    [Serializable]
    internal class PeerInfo
    {
        public string Address;
        public int AssignedJob;
        public IJobRunner JobRunner;
        public IJobTracker JobTracker;
        public INetworkManager NetworkManager;
        public bool RunnerAlive;
        public DateTime RunnerLastSeen;
        public Timer TrackerHeartbeatTimer;
        public bool TrackerAlive;
        public DateTime TrackerLastSeen;
        public Timer RunnerHeartbeatTimer;

        public bool Alive
        {
            get { return RunnerAlive && TrackerAlive; }
        }
    }
}