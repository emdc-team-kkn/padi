﻿using System;

namespace Worker.DataStructure
{
    [Serializable]
    public class Job
    {
        public readonly string ClientUri;
        public readonly int Id;
        public readonly long Offset;
        public readonly long Size;

        public Job(int id, long offset, long size, string clientUri)
        {
            ClientUri = clientUri;
            Id = id;
            Offset = offset;
            Size = size;
        }

        public override string ToString()
        {
            return string.Format("Id: {0}, Offset: {1}, Size: {2}, Client: {3}",
                Id, Offset, Size, ClientUri);
        }
    }
}