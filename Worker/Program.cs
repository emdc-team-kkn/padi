﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Threading;
using NLog;
using Worker.Business;

namespace Worker
{
    internal static class Program
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private static int Main(string[] args)
        {
            Log.Info("Worker started");

            if (args.Length == 0 || args.Length > 2)
            {
                Console.WriteLine("Invalid number of arguments.");
                return (int) ExitCode.InvalidArgumentsCount;
            }

            var uri = new Uri(args[0]);

            Log.Debug("Initializing worker");
            var config = new Dictionary<string, string>();
            config["port"] = uri.Port.ToString();
            config["name"] = "Worker";
            try
            {
                var channel = new TcpChannel(
                    config,
                    new BinaryClientFormatterSinkProvider(),
                    new BinaryServerFormatterSinkProvider()
                    );
                ChannelServices.RegisterChannel(channel, false);

                var repository = new DataRepository(uri);
                var jobRunner = new JobRunnerImpl(repository);
                var jobTracker = new JobTrackerImpl(repository, jobRunner);
                var networkManager = new NetworkManagerImpl(repository, jobRunner);
                var workerPuppet = new WorkerPuppetSvcImpl(repository, jobTracker, jobRunner, networkManager);

                RemotingServices.Marshal(networkManager, repository.BasePath + NetworkManagerImpl.Suffix);
                RemotingServices.Marshal(jobTracker, repository.BasePath + JobTrackerImpl.Suffix);
                RemotingServices.Marshal(jobRunner, repository.BasePath + JobRunnerImpl.Suffix);
                RemotingServices.Marshal(workerPuppet, repository.BasePath + WorkerPuppetSvcImpl.Suffix);

                Log.Debug("Worker service started at {0}", args[0]);
                if (args.Length > 1)
                {
                    Log.Debug("Sending registration request to {0}", args[1]);
                    networkManager.AddNewWorker(args[1]);
                    var peer =
                        (INetworkManager)
                            Activator.GetObject(typeof (INetworkManager), args[1] + NetworkManagerImpl.Suffix);
                    var token = Guid.NewGuid().ToString();
                    ThreadPool.QueueUserWorkItem(stateInfo =>
                    {
                        networkManager.AddToken(token);
                        try
                        {
                            peer.NewWorker(repository.ServiceUrl, token);
                        }
                        catch (Exception)
                        {
                            Log.Error("Unable to register ourself to {0}", args[1]);
                        }
                    });
                }

                Console.WriteLine("Please press Enter to exit");
                Console.ReadLine();
            }
            catch (SocketException e)
            {
                Log.Error("Unable to open socket", e);
                return (int) ExitCode.SocketException;
            }
            catch (Exception e)
            {
                Log.Error("Unhandle exception", e);
                return (int) ExitCode.UnhandleException;
            }

            return (int) ExitCode.Success;
        }

        private enum ExitCode
        {
            Success = 0,
            InvalidArgumentsCount = -1,
            UnhandleException = -2,
            SocketException = -3
        }
    }
}