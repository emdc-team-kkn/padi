﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using Common;
using NLog;
using Worker.DataStructure;

namespace Worker.Business
{
    internal delegate void AssignJobDelegate(PeerInfo worker, Job job);

    internal class JobScheduler
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly IDictionary<int, TimeSpan> _executionTime;
        private readonly Queue<Job> _jobs;
        private readonly IDictionary<int, JobInfo> _pendingJobs;
        private readonly string _serviceUri;
        private readonly Task _task;
        private readonly Queue<PeerInfo> _workers;
        private double _averageExecutionTime;
        private bool _freeze;
        private Timer _slowWorkerCheckerTimer;
        private bool _stop;
        private readonly HashSet<int> _finishedJobs;

        public JobScheduler(
            Task task,
            Queue<PeerInfo> workers,
            string serviceUrl
            )
        {
            _task = task;
            _workers = workers;
            _jobs = new Queue<Job>();
            _pendingJobs = new Dictionary<int, JobInfo>();
            _executionTime = new Dictionary<int, TimeSpan>();
            _finishedJobs = new HashSet<int>();
            _freeze = false;
            _serviceUri = serviceUrl;
            _averageExecutionTime = -1;
            _stop = false;
        }

        public bool Freeze
        {
            set
            {
                lock (this)
                {
                    _freeze = value;
                    Monitor.PulseAll(this);
                }
            }
            get { return _freeze; }
        }

        public void Start()
        {
            Log.Info("Running job scheduler for client: {0}", _task.ClientUri);
            Log.Debug("Splitting task into {0} job", _task.SplitCount);
            var splitSize = (_task.InputSize + _task.SplitCount - 1)/_task.SplitCount;
            var offset = 0L;
            var id = 1;
            while (offset < _task.InputSize)
            {
                var job = new Job(id, offset, splitSize, _task.ClientUri);
                _jobs.Enqueue(job);
                offset += splitSize;
                id += 1;
            }

            Log.Debug("Replicated task to all peers");
            var count = 0;
            while (count < _workers.Count)
            {
                var worker = _workers.Dequeue();
                try
                {
                    worker.JobRunner.SaveTask(_serviceUri, _task);
                    _workers.Enqueue(worker);
                }
                catch (Exception e)
                {
                    Log.Warn("Exception sending tasks to worker", e);
                }
                count++;
            }

            Log.Debug("Execution started, Workers' count: {0}, Jobs' count: {1}", _workers.Count, _jobs.Count);
            while (true)
            {
                lock (this)
                {
                    // If there're pending jobs, and no workers or jobs are available, we wait for the result
                    // of the pending job.
                    while ((_pendingJobs.Count != 0 && (_workers.Count == 0 || _jobs.Count == 0)))
                    {
                        if (_jobs.Count == 0 && _workers.Count > 0 && _slowWorkerCheckerTimer == null)
                        {
                            // All job have been assigned and we have some free node, start checking for slow node.
                            // We will check every 10 seconds
                            _slowWorkerCheckerTimer = new Timer(
                                CheckSlowWorkerCallback,
                                null, 0,
                                10000
                                );
                        }
                        Monitor.Wait(this);
                    }
                    // If the scheduler is stop, exit
                    if (_stop)
                    {
                        break;
                    }

                    // If there're no pending jobs and no queued jobs -> all jobs are finished, we can return
                    // If there're no pending jobs and no workers -> all workers have crashed, we can return
                    if (_pendingJobs.Count == 0 && (_workers.Count == 0 || _jobs.Count == 0))
                    {
                        break;
                    }

                    var job = _jobs.Dequeue();
                    // Skip over finished job
                    if (_finishedJobs.Contains(job.Id))
                    {
                        continue;
                    }
                    var worker = _workers.Dequeue();
                    
                    // Execute the job
                    var jobInfo = new JobInfo {StartTime = DateTime.Now, Job = job, Worker = worker};
                    var assignJobDelegate = new AssignJobDelegate(AssignJob);
                    assignJobDelegate.BeginInvoke(worker, job, AssignJobCallback, jobInfo);
                    _pendingJobs[job.Id] = jobInfo;    
                }
            }
            if (_slowWorkerCheckerTimer != null)
            {
                _slowWorkerCheckerTimer.Dispose();
            }
            var client = (IClientSvc)Activator.GetObject(typeof(IClientSvc), _task.ClientUri);
            if (!_stop)
            {
                Log.Info("Job scheduler complete successfully");
                client.ExecutionComplete(true);
            }
            else
            {
                Log.Warn("Job scheduler is stopped prematurely. Jobs left: {0}", _jobs.Count);
            }
        }

        public void StartRecovery()
        {
            Log.Debug("Recovery started");
            lock (this)
            {
                foreach (var worker in _workers)
                {
                    try
                    {
                        var finishedJobs = worker.JobRunner.FinishedJob(_task);
                        _finishedJobs.UnionWith(finishedJobs);
                    }
                    catch (Exception e)
                    {
                        Log.Debug("Exception when getting finished job", e);
                    }
                }
            }
            Log.Debug("Recovery done. Finished jobs count: {0}", _finishedJobs.Count);
            Start();
        }

        private void AssignJob(PeerInfo worker, Job job)
        {
            lock (this)
            {
                worker.AssignedJob = job.Id;
            }
            CheckFreeze();
            worker.JobRunner.ExecuteJob(job);    
        }

        private void AssignJobCallback(IAsyncResult ar)
        {
            var result = (AsyncResult) ar;
            var caller = (AssignJobDelegate) result.AsyncDelegate;
            var jobInfo = (JobInfo) ar.AsyncState;

            try
            {
                caller.EndInvoke(ar);
            }
            catch (Exception e)
            {
                Log.Warn("Worker node failure, requeing job", e);
                lock (this)
                {
                    if (_pendingJobs.ContainsKey(jobInfo.Job.Id))
                    {
                        _pendingJobs.Remove(jobInfo.Job.Id);
                    }
                    _jobs.Enqueue(jobInfo.Job);    
                }
            }
        }

        public void Stop()
        {
            lock (this)
            {
                _stop = true;
                Monitor.PulseAll(this);
            }
        }

        public void AddWorker(PeerInfo worker)
        {
            lock (this)
            {
                _workers.Enqueue(worker);
                Monitor.PulseAll(this);
            }
        }

        public bool ExecuteCallback(JobInfo jobInfo, string sender)
        {
            var ret = false;
            lock (this)
            {
                var success = jobInfo.Success;
                Log.Debug("Job info: {0}", jobInfo);
                var job = jobInfo.Job;
                if (_pendingJobs.ContainsKey(job.Id))
                {
                    var pendingJob = _pendingJobs[job.Id];
                    if (pendingJob.Worker.Address != sender)
                    {
                        Log.Debug("Sender: {0}, expecting from: {1}. Ignoring", sender, pendingJob.Worker.Address);
                    }
                    else
                    {
                        if (success || !_stop)
                        {
                            Log.Debug("Execution complete, start writing result");
                            _executionTime[job.Id] = DateTime.Now - pendingJob.StartTime;
                            _averageExecutionTime = _executionTime.Values.Average(num => num.Ticks);
                            ret = true;
                        }
                        else
                        {
                            Log.Debug("Execution error, requeue job & worker");
                            _jobs.Enqueue(job);
                            _workers.Enqueue(pendingJob.Worker);
                        }
                    }
                }
                else
                {
                    Log.Debug("Another worker has finished this job.");
                }
                Monitor.PulseAll(this);
            }
            return ret;
        }

        public void WriteCompleteCallback(JobInfo jobInfo)
        {
            var job = jobInfo.Job;
            lock (this)
            {
                var pendingJob = _pendingJobs[job.Id];
                if (jobInfo.Success)
                {
                    Log.Debug("Writing result complete, freeing worker");
                    _pendingJobs.Remove(job.Id);
                    _workers.Enqueue(pendingJob.Worker);
                }
                else
                {
                    Log.Debug("Write result error. Retrying");
                    pendingJob.Worker.JobRunner.WriteResult(jobInfo);
                }
                Monitor.PulseAll(this);
            }
        }

        public void RemoveWorker(PeerInfo worker)
        {
            lock (this)
            {
                if (_pendingJobs.ContainsKey(worker.AssignedJob))
                {
                    // Re queue the job of the dead workers
                    Log.Debug("Putting job {0} back into the queue", worker.AssignedJob);
                    var job = _pendingJobs[worker.AssignedJob];
                    _pendingJobs.Remove(worker.AssignedJob);
                    _jobs.Enqueue(job.Job);
                    Monitor.PulseAll(this);
                }
            }
        }

        /// <summary>
        ///     Check for slow workers.
        ///     Slow workers are workes that has execute for more than 2 time the average execution time.
        /// </summary>
        /// <param name="state"></param>
        private void CheckSlowWorkerCallback(object state)
        {
            lock (this)
            {
                var tobeRemove = new HashSet<int>();
                foreach (var item in _pendingJobs)
                {
                    var id = item.Key;
                    var pendingJob = item.Value;
                    var executionTime = DateTime.Now - pendingJob.StartTime;
                    if (_averageExecutionTime > 0 && executionTime.Ticks > 2*_averageExecutionTime)
                    {
                        Log.Debug("Slow worker detected: {0}", pendingJob.Worker.Address);
                        tobeRemove.Add(id);
                    }
                }
                foreach (var id in tobeRemove)
                {
                    Log.Debug("Put job {0} back into queue", id);
                    _jobs.Enqueue(_pendingJobs[id].Job);
                    _pendingJobs.Remove(id);
                }
                Monitor.PulseAll(this);
            }
        }

        private void CheckFreeze()
        {
            lock (this)
            {
                while (_freeze)
                {
                    Monitor.Wait(this);
                }
            }
        }
    }
}