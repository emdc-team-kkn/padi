﻿using Worker.DataStructure;

namespace Worker.Business
{
    internal interface INetworkManager
    {
        void NewWorker(string address, string token);
        void ViewChange(string sender, int round);
        void ViewChangeOk(string sender, int round);
        void Success(string sender, Decree decree, int round);
    }
}