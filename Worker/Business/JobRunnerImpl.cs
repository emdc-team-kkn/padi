﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using Common;
using NLog;
using Worker.DataStructure;

namespace Worker.Business
{
    internal delegate bool ProcessExecuteJobDelegate(Job j);

    internal delegate bool ProcessWriteResultDelegate(JobInfo jobInfo);

    internal class JobRunnerImpl : MarshalByRefObject, IJobRunner
    {
        public const string Suffix = ".rn";
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly DataRepository _repository;
        
        public JobRunnerImpl(DataRepository repository)
        {
            _repository = repository;
        }

        public void SaveTask(string coordinator, Task task)
        {
            lock (_repository)
            {
                CheckFreeze();
                var clients = _repository.Clients;
                if (!clients.ContainsKey(task.ClientUri))
                {
                    clients[task.ClientUri] = new ClientInfo();
                }
                clients[task.ClientUri].Task = task;
                clients[task.ClientUri].CoordinatorUri = coordinator;
            }
        }

        public void ExecuteJob(Job job)
        {
            CheckFreeze();
            Log.Debug("Job received: {0}", job);
            var executeJobDelegate = new ProcessExecuteJobDelegate(ProcessExecuteJob);
            executeJobDelegate.BeginInvoke(job, ProcessExecuteJobCallback,
                new JobInfo {Job = job, Success = false}
                );
        }

        public void WriteResult(JobInfo jobInfo)
        {
            CheckFreeze();
            var writeResultDelegate = new ProcessWriteResultDelegate(ProcessWriteResult);
            writeResultDelegate.BeginInvoke(jobInfo, ProcessWriteResultCallback, jobInfo);
        }

        public void Ping(string address)
        {
            lock (_repository)
            {
                CheckFreeze();
                var peers = _repository.Peers;
                if (peers.ContainsKey(address))
                {
                    // Update the ping time
                    peers[address].TrackerLastSeen = DateTime.Now;
                }
            }
        }

        public HashSet<int> FinishedJob(Task task)
        {
            lock (this)
            {
                return _repository.Clients[task.ClientUri].WrittenJob;    
            }
        }

        private bool ProcessExecuteJob(Job job)
        {
            var clients = _repository.Clients;
            var task = clients[job.ClientUri].Task;
            var assembly = Assembly.Load(task.Code);
            // Walk through each type in the assembly looking for our class
            foreach (var type in assembly.GetTypes())
            {
                if (type.IsClass)
                {
                    if (type.FullName.EndsWith("." + task.ClassName))
                    {
                        var clientSvc = (IClientSvc) Activator.GetObject(
                            typeof (IClientSvc), job.ClientUri
                            );
                        var mapper = Activator.CreateInstance(type);
                        if (!clients.ContainsKey(job.ClientUri))
                        {
                            clients[job.ClientUri] = new ClientInfo();
                        }

                        // Read the input into a temporary file
                        var path = Path.GetTempFileName();
                        using (var writer = new FileStream(path, FileMode.Open))
                        {
                            _repository.UpdateRunnerStatus(job.ClientUri, "Reading input");
                            // Use a buffer of 32 kB
                            const int bufferSize = 32*1024;
                            var offset = job.Offset;
                            var totalRead = 0;
                            while (true)
                            {
                                int readBytes, writeBytes = 0;
                                var buffer = clientSvc.ReadInput(offset, bufferSize, out readBytes);
                                if (readBytes == 0)
                                {
                                    break;
                                }
                                offset += readBytes;
                                totalRead += readBytes;
                                if (totalRead > job.Size)
                                {
                                    // Already read past the end, 
                                    // search for the first new line character after the end
                                    var decodedBuffer = Encoding.UTF8.GetString(buffer);
                                    int startPosition;
                                    if (readBytes + job.Size - totalRead < bufferSize)
                                    {
                                        // Handle the buffer which contain the last part of the split.
                                        startPosition = (int) (readBytes + job.Size - totalRead);
                                    }
                                    else
                                    {
                                        // This happen when we're unable to find new line on the last buffer
                                        // and have to keep reading
                                        startPosition = 0;
                                    }

                                    var newLine = decodedBuffer.IndexOf('\n', startPosition);
                                    if (newLine == -1)
                                    {
                                        writeBytes = readBytes;
                                    }
                                    else
                                    {
                                        writeBytes += newLine;
                                    }
                                }
                                else
                                {
                                    writeBytes = readBytes;
                                }
                                writer.Write(buffer, 0, writeBytes);
                                // We found the first new line character after the end
                                if (writeBytes < readBytes)
                                {
                                    break;
                                }
                            }
                        }

                        // Open the tempfile and call the mapper on each line
                        var ret = new Dictionary<string, IList<string>>();
                        using (var reader = new StreamReader(path))
                        {
                            _repository.UpdateRunnerStatus(job.ClientUri, "Running mapper");
                            string line;
                            // Skip the first line if we're not the first job
                            if (job.Id > 1)
                            {
                                reader.ReadLine();
                            }
                            while ((line = reader.ReadLine()) != null)
                            {
                                var args = new object[] {line};
                                var lineResult = (List<KeyValuePair<string, string>>) type.InvokeMember(
                                    "Map",
                                    BindingFlags.Default | BindingFlags.InvokeMethod,
                                    null,
                                    mapper,
                                    args
                                    );
                                foreach (var item in lineResult)
                                {
                                    if (!ret.ContainsKey(item.Key))
                                    {
                                        ret[item.Key] = new List<string>();
                                    }
                                    ret[item.Key].Add(item.Value);
                                }
                            }
                        }
                        if (_repository.SlowDown > 0)
                        {
                            Thread.Sleep(_repository.SlowDown*1000);
                        }
                        File.Delete(path);
                        clients[job.ClientUri].ResultCache[job.Id] = ret;
                        CheckFreeze();
                        return true;
                    }
                }
            }
            return false;
        }

        private void ProcessExecuteJobCallback(IAsyncResult ar)
        {
            var result = (AsyncResult) ar;
            var caller = (ProcessExecuteJobDelegate) result.AsyncDelegate;
            var jobInfo = (JobInfo) ar.AsyncState;
            try
            {
                jobInfo.Success = caller.EndInvoke(result);
                var clients = _repository.Clients;
                var coordinatorUri = clients[jobInfo.Job.ClientUri].CoordinatorUri;
                var coordinator = (IJobTracker) Activator.GetObject(
                    typeof (IJobTracker), coordinatorUri
                    );
                CheckFreeze();
                var writeAllowed = coordinator.ExecuteJobComplete(jobInfo, _repository.ServiceUrl);
                if (writeAllowed)
                {
                    WriteResult(jobInfo);
                }
            }
            catch (TimeoutException e)
            {
                Log.Warn("Timeout executing job", e);
            }
            catch (Exception e)
            {
                Log.Warn("Job tracker failure", e);
            }
        }

        private bool ProcessWriteResult(JobInfo jobInfo)
        {
            CheckFreeze();
            var job = jobInfo.Job;
            var clients = _repository.Clients;
            var ret = clients[job.ClientUri].ResultCache[job.Id];
            _repository.UpdateRunnerStatus(job.ClientUri, "Writing output");
            var clientSvc = (IClientSvc) Activator.GetObject(
                typeof (IClientSvc), job.ClientUri
                );
            try
            {
                Log.Debug("Start writing result");
                CheckFreeze();
                clientSvc.WriteOutput(jobInfo.Job.Id, ret.ToArray());
                Log.Debug("Write complete");
                _repository.UpdateRunnerStatus(job.ClientUri, null);
                clients[job.ClientUri].ResultCache.Remove(job.Id);
                return true;
            }
            catch (Exception e)
            {
                Log.Warn("Exception when sending result to client", e);
                return false;
            }
        }

        private void ProcessWriteResultCallback(IAsyncResult ar)
        {
            CheckFreeze();
            var jobInfo = (JobInfo) ar.AsyncState;
            var asyncResult = (AsyncResult) ar;
            var caller = (ProcessWriteResultDelegate) asyncResult.AsyncDelegate;
            
            try
            {
                jobInfo.Success = caller.EndInvoke(ar);
                var client = _repository.Clients[jobInfo.Job.ClientUri];
                lock (this)
                {
                    client.WrittenJob.Add(jobInfo.Job.Id);    
                }
                var coordinator = (IJobTracker) Activator.GetObject(
                    typeof (IJobTracker), client.CoordinatorUri
                    );
                CheckFreeze();
                coordinator.WriteResultComplete(jobInfo);
            }
            catch (Exception e)
            {
                Log.Warn("Job tracker failure", e);
            }
        }

        /// <summary>
        ///     Override to provide infinite lease time
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        private void CheckFreeze()
        {
            var startTime = DateTime.Now;
            var timeout = new TimeSpan(0, 0, 30);
            lock (_repository)
            {
                while (_repository.RunnerFreeze)
                {
                    Monitor.Wait(_repository, timeout);
                    var elapsed = DateTime.Now - startTime;
                    if (elapsed > timeout)
                    {
                        throw new TimeoutException();
                    }
                }
            }
        }
    }
}