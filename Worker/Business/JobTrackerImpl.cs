﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using NLog;
using Worker.DataStructure;

namespace Worker.Business
{
    internal class JobTrackerImpl : MarshalByRefObject, IJobTracker
    {
        public const string Suffix = "";
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly IJobRunner _jobRunner;
        private readonly DataRepository _repository;

        public JobTrackerImpl(DataRepository repository, IJobRunner jobRunner)
        {
            _repository = repository;
            _jobRunner = jobRunner;
        }

        public bool ExecuteJobComplete(JobInfo jobInfo, string sender)
        {
            var clients = _repository.Clients;
            var clientUri = jobInfo.Job.ClientUri;
            var scheduler = clients[clientUri].Scheduler;
            CheckFreeze();
            return scheduler.ExecuteCallback(jobInfo, sender);
        }

        public void WriteResultComplete(JobInfo jobInfo)
        {
            var clients = _repository.Clients;
            var clientUri = jobInfo.Job.ClientUri;
            var scheduler = clients[clientUri].Scheduler;
            CheckFreeze();
            scheduler.WriteCompleteCallback(jobInfo);
        }

        public bool SendMapper(byte[] code, string className, string clientUri, long inputSize, int splitCount)
        {
            Log.Info("Receiving task from {0}, size: {1}, split: {2}", clientUri, inputSize, splitCount);
            var assembly = Assembly.Load(code);
            // Walk through each type in the assembly looking for our class
            foreach (var type in assembly.GetTypes())
            {
                if (type.IsClass)
                {
                    if (type.FullName.EndsWith("." + className))
                    {
                        // Create the job and put it the queue
                        var t = new Task(className, clientUri, code, inputSize, splitCount);
                        ThreadPool.QueueUserWorkItem(stateInfo =>
                        {
                            var task = (Task) stateInfo;
                            var peers = _repository.Peers;
                            var clients = _repository.Clients;
                            // Create a worker queue include all peers and self
                            var workerQueue = new Queue<PeerInfo>(peers.Select(kv => kv.Value).AsEnumerable());
                            workerQueue.Enqueue(new PeerInfo
                            {
                                Address = _repository.ServiceUrl,
                                JobRunner = _jobRunner
                            });

                            // Create and store a new job scheduler for this client
                            clients[clientUri] = new ClientInfo
                            {
                                Scheduler = new JobScheduler(
                                    task, workerQueue, _repository.ServiceUrl)
                            };
                            _repository.UpdateTrackerStatus(clientUri, "Running");
                            clients[clientUri].StartScheduler();
                            // Job scheduler status
                            _repository.UpdateTrackerStatus(clientUri, null);
                        }, t);
                        CheckFreeze();
                        return true;
                    }
                }
            }
            return false;
        }

        public void Ping(string address)
        {
            lock (_repository)
            {
                var peers = _repository.Peers;
                if (peers.ContainsKey(address))
                {
                    // Update the ping time
                    peers[address].RunnerLastSeen = DateTime.Now;
                }
            }
        }

        /// <summary>
        ///     Override to provide infinite lease time
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        private void CheckFreeze()
        {
            var startTime = DateTime.Now;
            var timeout = new TimeSpan(0, 0, 30);
            lock (_repository)
            {
                while (_repository.TrackerFreeze)
                {
                    Monitor.Wait(_repository, timeout);
                    var elapsed = DateTime.Now - startTime;
                    if (elapsed > timeout)
                    {
                        throw new TimeoutException();
                    }
                }
            }
        }
    }
}