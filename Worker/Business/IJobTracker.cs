﻿using Common;
using Worker.DataStructure;

namespace Worker.Business
{
    internal interface IJobTracker : IWorkerSvc
    {
        bool ExecuteJobComplete(JobInfo jobInfo, string sender);
        void WriteResultComplete(JobInfo jobInfo);
        void Ping(string address);
    }
}