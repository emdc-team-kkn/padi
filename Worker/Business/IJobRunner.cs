﻿using System.Collections.Generic;
using Worker.DataStructure;

namespace Worker.Business
{
    internal interface IJobRunner
    {
        void SaveTask(string coordinatorUri, Task task);
        void ExecuteJob(Job job);
        void WriteResult(JobInfo jobInfo);
        void Ping(string address);
        HashSet<int> FinishedJob(Task task);
    }
}