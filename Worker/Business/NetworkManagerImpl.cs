﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NLog;
using Worker.DataStructure;

namespace Worker.Business
{
    internal class NetworkManagerImpl : MarshalByRefObject, INetworkManager
    {
        public const String Suffix = ".nm";
        private const int HeartBeatLapse = 15*1000;
        private const int HealthCheckLapse = 15*1000;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly HashSet<string> _forwardToken;
        private readonly Timer _healthCheckTimer;
        private readonly DataRepository _repository;
        private readonly IJobRunner _jobRunner;

        private Dictionary<int, bool> _voted = new Dictionary<int, bool>();
        private int _viewChangeVoteCount = 0;
        private int _round = 0;

        public NetworkManagerImpl(DataRepository repository, IJobRunner jobRunner)
        {
            _repository = repository;
            _forwardToken = new HashSet<string>();
            
            _healthCheckTimer = new Timer(CheckHealthyPeer, null, HealthCheckLapse, HealthCheckLapse);
            _jobRunner = jobRunner;
        }

        public void NewWorker(string address, string token)
        {
            CheckFreezeRunner();
            lock (_repository)
            {
                var peers = _repository.Peers;
                if (!_forwardToken.Contains(token))
                {
                    // New request, forward it
                    Log.Debug("Registration token: {0}", token);
                    foreach (var peer in peers.Values)
                    {
                        if (peer.Address != address)
                        {
                            Log.Debug("Forwarding new worker message to {0}", peer.Address);
                            ThreadPool.QueueUserWorkItem(stateInfo =>
                            {
                                var p = (PeerInfo) stateInfo;
                                CheckFreezeRunner();
                                try
                                {
                                    p.NetworkManager.NewWorker(address, token);
                                }
                                catch (Exception e)
                                {
                                    Log.Debug("Unable to forward new worker message", e);
                                }
                            }, peer);
                        }
                    }

                    // Save the token so we don't forward it again
                    _forwardToken.Add(token);
                }

                // This is a new peer
                if (!peers.ContainsKey(address))
                {
                    // Add the remote peer peer to our list
                    AddNewWorker(address);
                    // Add ourself to its list
                    ThreadPool.QueueUserWorkItem(stateInfo =>
                    {
                        var other = (INetworkManager) Activator.GetObject(typeof (INetworkManager),
                            address + Suffix
                            );
                        CheckFreezeRunner();
                        try
                        {
                            other.NewWorker(_repository.ServiceUrl, token);
                        }
                        catch (Exception e)
                        {
                            Log.Debug("Unable to introduce ourself to new peer", e);
                        }
                    });
                }
                else
                {
                    peers[address].RunnerAlive = true;
                    peers[address].TrackerAlive = true;
                    peers[address].RunnerLastSeen = DateTime.Now;
                    peers[address].TrackerLastSeen = DateTime.Now;
                }
            }
        }

        public void AddNewWorker(string address)
        {
            lock (_repository)
            {
                var peers = _repository.Peers;
                var clients = _repository.Clients;

                // New peers, store it in our list
                Log.Debug("New worker at: {0}", address);
                var peerNm = (INetworkManager) Activator.GetObject(typeof (INetworkManager), address + Suffix);
                var jobRunner = (IJobRunner) Activator.GetObject(typeof (IJobRunner), address + JobRunnerImpl.Suffix);
                var jobTracker =
                    (IJobTracker) Activator.GetObject(typeof (IJobTracker), address + JobTrackerImpl.Suffix);
                peers[address] = new PeerInfo
                {
                    JobRunner = jobRunner,
                    JobTracker = jobTracker,
                    NetworkManager = peerNm,
                    Address = address,
                    TrackerHeartbeatTimer = new Timer(TrackerHeartBeat, jobRunner, 0, HeartBeatLapse),
                    RunnerHeartbeatTimer = new Timer(RunnerHeartBeat, jobTracker, 0, HeartBeatLapse),
                    RunnerAlive = true,
                    TrackerAlive = true,
                    RunnerLastSeen = DateTime.Now,
                    TrackerLastSeen = DateTime.Now
                };
                foreach (var c in clients)
                {
                    if (c.Value.Scheduler != null)
                    {
                        c.Value.Scheduler.AddWorker(peers[address]);
                    }
                }
            }
        }

        private void TrackerHeartBeat(object state)
        {
            var runner = (IJobRunner) state;
            try
            {
                if (!_repository.TrackerFreeze)
                {
                    CheckFreezeTracker();
                    runner.Ping(_repository.ServiceUrl);
                }
            }
            catch (Exception)
            {
                Log.Debug("Exception when pinging runner");
            }
        }

        private void RunnerHeartBeat(object state)
        {
            var tracker = (IJobTracker) state;
            try
            {
                if (!_repository.RunnerFreeze)
                {
                    CheckFreezeRunner();
                    tracker.Ping(_repository.ServiceUrl);
                }
            }
            catch (Exception)
            {
                Log.Debug("Exception when pinging tracker");
            }
        }

        private void CheckHealthyPeer(object state)
        {
            lock (_repository)
            {
                var peers = _repository.Peers;
                var clients = _repository.Clients;
                var deadRunnerCounts = peers.Count(kv => !kv.Value.RunnerAlive);
                foreach (var p in peers)
                {
                    var peerInfo = p.Value;
                    // Check for alive runner
                    var runnerElapsedTime = DateTime.Now - peerInfo.RunnerLastSeen;
                    if (peerInfo.RunnerAlive && runnerElapsedTime.TotalMilliseconds > 2*HeartBeatLapse)
                    {
                        Log.Warn("Job runner at {0} is death, removing", peerInfo.Address);
                        peerInfo.RunnerAlive = false;
                        deadRunnerCounts += 1;
                        foreach (var c in clients)
                        {
                            var clientInfo = c.Value;
                            if (clientInfo.Scheduler != null)
                            {
                                // We are the job tracker
                                clientInfo.Scheduler.RemoveWorker(peerInfo);
                                Log.Debug("Total peers: {0}, Healthy peers: {1}",
                                    peers.Count,
                                    peers.Count - deadRunnerCounts
                                    );
                                if (peers.Count - deadRunnerCounts < deadRunnerCounts)
                                {
                                    Log.Warn("Not enough peers to maintain consistency. Stopping scheduler");
                                    clientInfo.Scheduler.Stop();
                                    _repository.UpdateTrackerStatus(clientInfo.Task.ClientUri, null);
                                }
                            }
                        }
                    }
                    else if (!peerInfo.RunnerAlive && runnerElapsedTime.TotalMilliseconds <= 2 * HeartBeatLapse)
                    {
                        Log.Debug("Job runner at {0} come back to live", peerInfo.Address);
                        peerInfo.RunnerAlive = true;
                        foreach (var c in clients)
                        {
                            var clientInfo = c.Value;
                            if (clientInfo.Scheduler != null)
                            {
                                clientInfo.Scheduler.AddWorker(peerInfo);
                            }
                        }
                    }

                    // Check for alive tracker
                    var trackerElapseTime = DateTime.Now - peerInfo.TrackerLastSeen;
                    if (peerInfo.TrackerAlive && trackerElapseTime.TotalMilliseconds > 2*HeartBeatLapse)
                    {
                        Log.Warn("Job tracker at {0} is death, removing", peerInfo.Address);
                        peerInfo.TrackerAlive = false;

                    }
                    else if (!peerInfo.TrackerAlive && trackerElapseTime.TotalMilliseconds <= 2 * HeartBeatLapse)
                    {
                        Log.Debug("Job tracker at {0} come back to live", peerInfo.Address);
                        peerInfo.TrackerAlive = true;
                    }
                }
                var orphanClients = new HashSet<string>();
                foreach (var p in peers)
                {
                    var peerInfo = p.Value;
                    if (!peerInfo.TrackerAlive)
                    {
                        foreach (var c in clients)
                        {
                            if (c.Value.CoordinatorUri == peerInfo.Address)
                            {
                                Log.Debug("Client {0} orphaned", c.Key);
                                orphanClients.Add(c.Key);
                            }
                        }
                    }
                }
                
                Log.Debug("Healthy runner left: {0}", peers.Count - deadRunnerCounts + 1);
                Log.Debug("Orphaned clients: {0}", orphanClients.Count);
                Log.Debug("Negotiate: {0}",
                    orphanClients.Count > 0 && peers.Count - deadRunnerCounts >= deadRunnerCounts);
                if (orphanClients.Count > 0 && peers.Count - deadRunnerCounts >= deadRunnerCounts)
                {
                    NegotiateNewTracker();
                }
            }
        }

        /// <summary>
        ///     Override to provide infinite lease time
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        private void NegotiateNewTracker()
        {
            lock (_repository)
            {
                // Check if we should be the president
                var isPresident = true;
                var peers = _repository.Peers;
                foreach (var p in peers.Where(kv => kv.Value.Alive))
                {
                    var peerInfo = p.Value;
                    Log.Debug("Checking peer: {0}, tracker {1}, runner {2}", 
                        peerInfo.Address, peerInfo.TrackerAlive, peerInfo.RunnerAlive);
                    if (String.Compare(_repository.ServiceUrl, peerInfo.Address, StringComparison.Ordinal) < 0)
                    {
                        isPresident = false;
                    }
                }
                if (!isPresident)
                {
                    return;
                }
                Log.Info("I am the president. Starting negotiation for new coordinator");
                _viewChangeVoteCount = 1;
                _round += 1;
                _voted[_round] = true;
                foreach (var p in peers.Where(kv => kv.Value.RunnerAlive))
                {
                    try
                    {
                        CheckFreezeRunner();
                        p.Value.NetworkManager.ViewChange(_repository.ServiceUrl, _round);
                    }
                    catch (Exception)
                    {
                        Log.Debug("Error sending message to: {0}", p.Value.Address);
                    }
                }    
            }
        }

        public void ViewChange(string sender, int round)
        {
            CheckFreezeRunner();
            ThreadPool.QueueUserWorkItem(stateInfo =>
            {
                lock (_repository)
                {
                    var peer = _repository.Peers[sender];
                    if (peer.TrackerAlive && round > _round && 
                            (!_voted.ContainsKey(round) || !_voted[round]))
                    {
                        _voted[round] = true;
                        peer.NetworkManager.ViewChangeOk(_repository.ServiceUrl, round);
                    }
                }
            });
        }

        public void ViewChangeOk(string sender, int round)
        {
            CheckFreezeRunner();
            ThreadPool.QueueUserWorkItem(stateInfo =>
            {
                lock (_repository)
                {
                    if (_round != round)
                    {
                        return;
                    }
                    _viewChangeVoteCount += 1;
                    Log.Debug("Vote collected: {0}", _viewChangeVoteCount);    
                    if (_viewChangeVoteCount > _repository.Peers.Count/2)
                    {
                        foreach (var client in _repository.Clients.Values)
                        {
                            if (_repository.ServiceUrl != client.CoordinatorUri && 
                                !_repository.Peers[client.CoordinatorUri].TrackerAlive)
                            {
                                var address = client.Task.ClientUri;
                                var decree = new Decree(address, _repository.ServiceUrl);
                                WriteDecree(decree);
                                foreach (var p in _repository.Peers.Where(p => p.Value.RunnerAlive))
                                {
                                    var peer = p.Value;
                                    CheckFreezeRunner();
                                    peer.NetworkManager.Success(_repository.ServiceUrl, decree, round);
                                }    
                            }
                        }
                    }
                    _voted.Remove(round);
                }
            });
        }

        public void Success(string sender, Decree decree, int round)
        {
            ThreadPool.QueueUserWorkItem(stateInfo =>
            {
                lock (_repository)
                {
                    if (decree.Coordinator == _repository.ServiceUrl)
                    {
                        // If we are the coordinator, then stop the scheduler
                        _repository.Clients[decree.Client].Scheduler.Stop();
                    }
                    WriteDecree(decree);
                    _voted.Remove(round);
                    _round = round;
                }
            });
        }

        private void WriteDecree(Decree decree)
        {
            lock (_repository)
            {
                var clients = _repository.Clients;
                var peers = _repository.Peers;

                if (clients.ContainsKey(decree.Client))
                {
                    var clientInfo = clients[decree.Client];
                    Log.Info("Changing coordinator of {0} to: {1}", decree.Client, decree.Coordinator);
                    // If we're the old coordinator, stop it
                    if (clientInfo.Scheduler != null)
                    {
                        clientInfo.Scheduler.Stop();
                        _repository.UpdateTrackerStatus(decree.Client, null);
                    }
                    // Update the coordinator
                    clientInfo.CoordinatorUri = decree.Coordinator;
                    // Startup the new coordinator
                    if (decree.Coordinator == _repository.ServiceUrl)
                    {
                        ThreadPool.QueueUserWorkItem(stateInfo =>
                        {
                            var client = (ClientInfo)stateInfo;
                            var workerQueue = new Queue<PeerInfo>(peers.Select(kv => kv.Value).Where(p => p.RunnerAlive));
                            workerQueue.Enqueue(new PeerInfo
                            {
                                Address = _repository.ServiceUrl,
                                JobRunner = _jobRunner
                            });
                            // Only start the job scheduler if we have enough runner to maintain consistency
                            if (workerQueue.Count > peers.Count / 2)
                            {
                                Log.Debug("Starting up new job scheduler, worker count: {0}", workerQueue.Count);
                                client.Scheduler = new JobScheduler(client.Task, workerQueue, _repository.ServiceUrl);
                                _repository.UpdateTrackerStatus(client.Task.ClientUri, "Running");
                                client.Scheduler.StartRecovery();
                                _repository.UpdateTrackerStatus(client.Task.ClientUri, null);
                            }
                        }, clientInfo);
                    }
                }
            }
        }

        public void AddToken(string token)
        {
            lock (this)
            {
                _forwardToken.Add(token);
            }
        }

        private void CheckFreezeRunner()
        {
            var startTime = DateTime.Now;
            var timeout = new TimeSpan(0, 0, 30);
            lock (_repository)
            {
                while (_repository.RunnerFreeze)
                {
                    Monitor.Wait(_repository, timeout);
                    var elapsed = DateTime.Now - startTime;
                    if (elapsed > timeout)
                    {
                        throw new TimeoutException();
                    }
                }
            }
        }

        private void CheckFreezeTracker()
        {
            var timeout = new TimeSpan(0, 0, 30);
            var startTime = DateTime.Now;
            lock (_repository)
            {
                while (_repository.TrackerFreeze)
                {
                    Monitor.Wait(_repository, timeout);
                    var elapsed = DateTime.Now - startTime;
                    if (elapsed > timeout)
                    {
                        Log.Debug("Elapsed time: {0}, timeout: {1}", elapsed, timeout);
                        throw new TimeoutException();
                    }
                }
            }
            
        }
    }
}