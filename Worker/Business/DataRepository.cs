﻿using System;
using System.Collections.Generic;
using System.Threading;
using Worker.DataStructure;

namespace Worker.Business
{
    internal class DataRepository
    {
        public readonly IDictionary<string, ClientInfo> Clients;
        public readonly IDictionary<string, PeerInfo> Peers;
        public int SlowDown;
        public readonly Dictionary<string, string> TrackerStatuses;
        public readonly Dictionary<string, string> RunnerStatuses;
        private readonly Uri _nodeUri;
        private bool _runnerFreeze;
        private bool _trackerFreeze;
        

        public DataRepository(Uri uri)
        {
            _nodeUri = uri;
            SlowDown = 0;
            Peers = new Dictionary<string, PeerInfo>();
            Clients = new Dictionary<string, ClientInfo>();
            TrackerStatuses = new Dictionary<string, string>();
            RunnerStatuses = new Dictionary<string, string>();

        }

        public string ServiceUrl
        {
            get { return _nodeUri.ToString(); }
        }

        public string BasePath
        {
            get { return _nodeUri.Segments[1]; }
        }

        public bool RunnerFreeze
        {
            get { return _runnerFreeze; }
            set {
                lock (this)
                {
                    _runnerFreeze = value;
                    Monitor.PulseAll(this);
                }
            }
        }

        public bool TrackerFreeze
        {
            get { return _trackerFreeze; }
            set {
                lock (this)
                {
                    _trackerFreeze = value;
                    foreach (var client in Clients)
                    {
                        if (client.Value.Scheduler != null)
                        {
                            client.Value.Scheduler.Freeze = value;
                        }
                    }
                    Monitor.PulseAll(this);
                } 
            }
        }

        public void UpdateTrackerStatus(string clientUri, string status)
        {
            lock (this)
            {
                if (status == null)
                {
                    TrackerStatuses.Remove(clientUri);
                }
                else
                {
                    TrackerStatuses[clientUri] = status;    
                }
            }
        }

        public void UpdateRunnerStatus(string clientUri, string status)
        {
            lock (this)
            {
                if (status == null)
                {
                    RunnerStatuses.Remove(clientUri);
                }
                else
                {
                    RunnerStatuses[clientUri] = status;    
                }
            }
        }
    }
}