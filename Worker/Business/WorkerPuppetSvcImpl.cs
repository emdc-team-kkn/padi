﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using NLog;

namespace Worker.Business
{
    internal class WorkerPuppetSvcImpl : MarshalByRefObject, IWorkerPuppetSvc
    {
        public const string Suffix = ".pp";
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly JobRunnerImpl _jobRunner;
        private readonly JobTrackerImpl _jobTracker;
        private readonly NetworkManagerImpl _networkManager;
        private readonly DataRepository _repository;

        public WorkerPuppetSvcImpl(
            DataRepository repository,
            JobTrackerImpl jobTracker,
            JobRunnerImpl jobRunner,
            NetworkManagerImpl networkManager
            )
        {
            _repository = repository;
            _jobTracker = jobTracker;
            _jobRunner = jobRunner;
            _networkManager = networkManager;
        }

        public bool FreezeW
        {
            set
            {
                _repository.RunnerFreeze = value;    
            }
        }

        public bool FreezeC
        {
            set
            {
                _repository.TrackerFreeze = value;
            }
        }

        public void Status()
        {
            Log.Debug("Printing worker status");
            lock (_repository)
            {
                // Build the status
                var builder = new StringBuilder();
                var seperator = new string('=', 80);
                builder.AppendLine(seperator);
                builder.AppendLine(string.Format("Time: {0}", DateTime.Now));
                // Tracker status
                if (_repository.TrackerStatuses.Count == 0)
                {
                    builder.AppendLine("Job tracker status: Idling");
                }
                else
                {
                    builder.AppendLine("Job tracker status: ");
                    foreach (var item in _repository.TrackerStatuses)
                    {
                        var client = item.Key;
                        var status = item.Value;
                        builder.AppendFormat("Client: {0}, State: {1}\r\n", client, status);
                    }    
                }
                // Runner status
                if (_repository.RunnerStatuses.Count == 0)
                {
                    builder.AppendLine("Job runner status: Idling");
                }
                else
                {
                    builder.AppendLine("Job runner status: ");
                    foreach (var item in _repository.RunnerStatuses)
                    {
                        var client = item.Key;
                        var status = item.Value;
                        builder.AppendFormat("Client: {0}, State: {1}\r\n", client, status);
                    }
                }
                
                // Print peers status
                builder.AppendLine("Peers status:");
                foreach (var p in _repository.Peers)
                {
                    var peer = p.Value;
                    builder.AppendLine(
                        string.Format("{0}, alive tracker: {1}, alive worker: {2}", 
                            peer.Address, peer.TrackerAlive, peer.RunnerAlive));
                    builder.AppendLine(
                        string.Format("Last seen: tracker {0}, worker: {1}",
                            peer.TrackerLastSeen, peer.RunnerLastSeen));
                }

                builder.Append(seperator);
                // Write the status
                Console.WriteLine(builder.ToString());
            }
        }

        public int SlowWDuration
        {
            set
            {
                lock (_repository)
                {
                    _repository.SlowDown = value;
                }
            }
        }

        /// <summary>
        ///     Override to provide infinite lease time
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}