﻿namespace PuppetMaster.DataStructure
{
    internal class SubmitCommand
    {
        public readonly string Dll;
        public readonly string EntryUrl;
        public readonly string File;
        public readonly string Map;
        public readonly string Output;
        public readonly int Split;

        public SubmitCommand(string entryUrl, string file, string output, int split, string map, string dll)
        {
            EntryUrl = entryUrl;
            File = file;
            Output = output;
            Split = split;
            Map = map;
            Dll = dll;
        }

        public override string ToString()
        {
            return string.Format("SUBMIT {0} {1} {2} {3} {4} {5}",
                EntryUrl, File, Output, Split, Map, Dll);
        }
    }
}