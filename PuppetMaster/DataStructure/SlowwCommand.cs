﻿namespace PuppetMaster.DataStructure
{
    internal class SlowwCommand
    {
        public readonly int Delay;
        public readonly int Id;

        public SlowwCommand(int id, int delay)
        {
            Id = id;
            Delay = delay;
        }

        public override string ToString()
        {
            return string.Format("SLOWW {0} {1}", Id, Delay);
        }
    }
}