﻿using System;

namespace PuppetMaster.DataStructure
{
    /// <summary>
    ///     This class represent a WORKER command in a script
    /// </summary>
    internal class WorkerCommand
    {
        public readonly string EntryUrl;
        public readonly int Id;
        public readonly string PuppetMasterUrl;
        public readonly string ServiceUrl;

        public WorkerCommand(int id, string puppetMasterUrl, string serviceUrl, string entryUrl)
        {
            Id = id;
            PuppetMasterUrl = puppetMasterUrl;
            ServiceUrl = serviceUrl;
            EntryUrl = entryUrl;
        }

        public override string ToString()
        {
            return String.Format("WORKER {0} {1} {2} {3}",
                Id, PuppetMasterUrl, ServiceUrl, EntryUrl);
        }
    }
}