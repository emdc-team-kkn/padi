﻿using System;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Windows.Forms;
using NLog;
using PuppetMaster.Business;
using PuppetMaster.Properties;

namespace PuppetMaster.Gui
{
    internal delegate void RunScriptDelegate(string filePath);

    internal delegate bool RunCommandDelegate(string command);

    public partial class MainWindow : Form
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly ScriptParser _scriptParser;

        public MainWindow(ScriptParser scriptParser)
        {
            InitializeComponent();
            StatusBar.Renderer = new EllipsisStatusBarRenderer();
            _scriptParser = scriptParser;
            _scriptParser.ParseCompleteEvent += SetProgressBarMaxValue;
            _scriptParser.CommandExecutedEvent += SetProgressBarValue;
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            Log.Info("GUI loaded");
        }

        private void LoadScriptBtn_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog {Multiselect = false};
            var dialogResult = openFileDialog.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                Log.Info("Script selected: {0}", openFileDialog.FileName);
                ScriptPathInput.Text = openFileDialog.FileName;
            }
        }

        private void RunScriptBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists(ScriptPathInput.Text))
            {
                _scriptParser.Debug = false;
                Log.Info("Executing script: {0}", ScriptPathInput.Text);
                var runScriptDelegate = new RunScriptDelegate(_scriptParser.LoadAndExecute);
                DisableInput();
                ProgressBar.Visible = true;
                StatusLabel.Text = string.Format("Executing script: {0}", ScriptPathInput.Text);
                runScriptDelegate.BeginInvoke(ScriptPathInput.Text, RunScriptCallback, null);
            }
            else
            {
                StatusLabel.Text = Resources.InvalidScriptPath;
            }
        }

        private void RunScriptCallback(IAsyncResult ar)
        {
            var result = (AsyncResult) ar;
            var caller = (RunScriptDelegate) result.AsyncDelegate;

            caller.EndInvoke(ar);
            Log.Info("Script execution completed");
            Invoke((MethodInvoker) delegate
            {
                EnableInput();
                StatusLabel.Text = Resources.Ready;
                ProgressBar.Visible = false;
            });
        }

        private void DisableInput()
        {
            DisableScript();
            DisableCommand();
        }

        private void DisableCommand()
        {
            CmdInput.Enabled = false;
            RunCmdBtn.Enabled = false;
        }

        private void DisableScript()
        {
            LoadScriptBtn.Enabled = false;
            RunScriptBtn.Enabled = false;
            DebugScriptBtn.Enabled = false;
            ScriptPathInput.Enabled = false;
        }

        private void EnableInput()
        {
            EnableScript();
            EnableCommand();
        }

        private void EnableCommand()
        {
            CmdInput.Enabled = true;
            RunCmdBtn.Enabled = true;
        }

        private void EnableScript()
        {
            LoadScriptBtn.Enabled = true;
            RunScriptBtn.Enabled = true;
            DebugScriptBtn.Enabled = true;
            ScriptPathInput.Enabled = true;
        }

        private void RunCmdBtn_Click(object sender, EventArgs e)
        {
            var command = CmdInput.Text.Trim();
            var runCommandElegate = new RunCommandDelegate(_scriptParser.ParseAndExecute);
            DisableCommand();
            ProgressBar.Visible = true;
            StatusLabel.Text = string.Format("Executing command: {0}", command);
            runCommandElegate.BeginInvoke(command, RunCommandCallback, null);
        }

        private void RunCommandCallback(IAsyncResult ar)
        {
            var result = (AsyncResult) ar;
            var caller = (RunCommandDelegate) result.AsyncDelegate;

            var ret = caller.EndInvoke(ar);
            Log.Info(ret ? "Execution completed" : "Invalid command");

            Invoke((MethodInvoker) delegate
            {
                EnableCommand();
                StatusLabel.Text = Resources.Ready;
                ProgressBar.Visible = false;
            });
        }

        private void SetProgressBarMaxValue(int maxValue)
        {
            Invoke((MethodInvoker) delegate { ProgressBar.Maximum = maxValue; });
        }

        private void SetProgressBarValue(int value)
        {
            Invoke((MethodInvoker) delegate { ProgressBar.Value = value; });
        }

        private void DebugScriptBtn_Click(object sender, EventArgs e)
        {
            _scriptParser.Debug = true;
            if (File.Exists(ScriptPathInput.Text))
            {
                Log.Info("Debugging script: {0}", ScriptPathInput.Text);
                var runScriptDelegate = new RunScriptDelegate(_scriptParser.LoadAndExecute);
                DisableScript();
                EnableDebugBtn();
                ProgressBar.Visible = true;
                StatusLabel.Text = string.Format("Executing script: {0}", ScriptPathInput.Text);
                runScriptDelegate.BeginInvoke(ScriptPathInput.Text, DebugScriptCallback, null);
            }
            else
            {
                StatusLabel.Text = Resources.InvalidScriptPath;
            }
        }

        private void DebugScriptCallback(IAsyncResult ar)
        {
            var result = (AsyncResult)ar;
            var caller = (RunScriptDelegate)result.AsyncDelegate;

            caller.EndInvoke(ar);
            Log.Info("Script execution completed");
            Invoke((MethodInvoker)delegate
            {
                EnableScript();
                DisableDebugBtn();
                StatusLabel.Text = Resources.Ready;
                ProgressBar.Visible = false;
            });
        }

        private void EnableDebugBtn()
        {
            ContinueBtn.Enabled = true;
            StopBtn.Enabled = true;
        }

        private void DisableDebugBtn()
        {
            ContinueBtn.Enabled = false;
            StopBtn.Enabled = false;
        }

        private void ContinueBtn_Click(object sender, EventArgs e)
        {
            _scriptParser.Continue();
        }

        private void StopBtn_Click(object sender, EventArgs e)
        {
            _scriptParser.Stop();
        }
    }
}