﻿using System.ComponentModel;
using System.Windows.Forms;

namespace PuppetMaster.Gui
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CmdInput = new System.Windows.Forms.TextBox();
            this.RunCmdBtn = new System.Windows.Forms.Button();
            this.OutputTxtBox = new System.Windows.Forms.TextBox();
            this.ScriptPathInput = new System.Windows.Forms.TextBox();
            this.LoadScriptBtn = new System.Windows.Forms.Button();
            this.RunScriptBtn = new System.Windows.Forms.Button();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.StopBtn = new System.Windows.Forms.Button();
            this.ContinueBtn = new System.Windows.Forms.Button();
            this.DebugScriptBtn = new System.Windows.Forms.Button();
            this.StatusBar.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CmdInput
            // 
            this.CmdInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CmdInput.Location = new System.Drawing.Point(3, 34);
            this.CmdInput.Name = "CmdInput";
            this.CmdInput.Size = new System.Drawing.Size(456, 20);
            this.CmdInput.TabIndex = 0;
            // 
            // RunCmdBtn
            // 
            this.RunCmdBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RunCmdBtn.Location = new System.Drawing.Point(465, 32);
            this.RunCmdBtn.Name = "RunCmdBtn";
            this.RunCmdBtn.Size = new System.Drawing.Size(75, 23);
            this.RunCmdBtn.TabIndex = 1;
            this.RunCmdBtn.Text = "Run";
            this.RunCmdBtn.UseVisualStyleBackColor = true;
            this.RunCmdBtn.Click += new System.EventHandler(this.RunCmdBtn_Click);
            // 
            // OutputTxtBox
            // 
            this.OutputTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OutputTxtBox.Location = new System.Drawing.Point(3, 61);
            this.OutputTxtBox.Multiline = true;
            this.OutputTxtBox.Name = "OutputTxtBox";
            this.OutputTxtBox.ReadOnly = true;
            this.OutputTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.OutputTxtBox.Size = new System.Drawing.Size(708, 228);
            this.OutputTxtBox.TabIndex = 2;
            // 
            // ScriptPathInput
            // 
            this.ScriptPathInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ScriptPathInput.Location = new System.Drawing.Point(3, 5);
            this.ScriptPathInput.Name = "ScriptPathInput";
            this.ScriptPathInput.Size = new System.Drawing.Size(456, 20);
            this.ScriptPathInput.TabIndex = 3;
            // 
            // LoadScriptBtn
            // 
            this.LoadScriptBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LoadScriptBtn.Location = new System.Drawing.Point(465, 3);
            this.LoadScriptBtn.Name = "LoadScriptBtn";
            this.LoadScriptBtn.Size = new System.Drawing.Size(75, 23);
            this.LoadScriptBtn.TabIndex = 4;
            this.LoadScriptBtn.Text = "Load Script";
            this.LoadScriptBtn.UseVisualStyleBackColor = true;
            this.LoadScriptBtn.Click += new System.EventHandler(this.LoadScriptBtn_Click);
            // 
            // RunScriptBtn
            // 
            this.RunScriptBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RunScriptBtn.Location = new System.Drawing.Point(546, 3);
            this.RunScriptBtn.Name = "RunScriptBtn";
            this.RunScriptBtn.Size = new System.Drawing.Size(80, 23);
            this.RunScriptBtn.TabIndex = 5;
            this.RunScriptBtn.Text = "Run Script";
            this.RunScriptBtn.UseVisualStyleBackColor = true;
            this.RunScriptBtn.Click += new System.EventHandler(this.RunScriptBtn_Click);
            // 
            // StatusBar
            // 
            this.StatusBar.Dock = System.Windows.Forms.DockStyle.None;
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.ProgressBar});
            this.StatusBar.Location = new System.Drawing.Point(0, 0);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.ShowItemToolTips = true;
            this.StatusBar.Size = new System.Drawing.Size(714, 22);
            this.StatusBar.TabIndex = 6;
            this.StatusBar.Text = "Status bar";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoToolTip = true;
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(699, 17);
            this.StatusLabel.Spring = true;
            this.StatusLabel.Text = "Ready";
            this.StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ProgressBar
            // 
            this.ProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(100, 16);
            this.ProgressBar.Visible = false;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.StatusBar);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.AutoScroll = true;
            this.toolStripContainer1.ContentPanel.Controls.Add(this.StopBtn);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.ContinueBtn);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.DebugScriptBtn);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.RunScriptBtn);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.LoadScriptBtn);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.ScriptPathInput);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.OutputTxtBox);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.RunCmdBtn);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.CmdInput);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(714, 292);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(714, 314);
            this.toolStripContainer1.TabIndex = 7;
            this.toolStripContainer1.Text = "toolStripContainer1";
            this.toolStripContainer1.TopToolStripPanelVisible = false;
            // 
            // StopBtn
            // 
            this.StopBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StopBtn.Enabled = false;
            this.StopBtn.Location = new System.Drawing.Point(632, 31);
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(79, 23);
            this.StopBtn.TabIndex = 8;
            this.StopBtn.Text = "Stop";
            this.StopBtn.UseVisualStyleBackColor = true;
            this.StopBtn.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // ContinueBtn
            // 
            this.ContinueBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ContinueBtn.Enabled = false;
            this.ContinueBtn.Location = new System.Drawing.Point(546, 31);
            this.ContinueBtn.Name = "ContinueBtn";
            this.ContinueBtn.Size = new System.Drawing.Size(80, 23);
            this.ContinueBtn.TabIndex = 7;
            this.ContinueBtn.Text = "Continue";
            this.ContinueBtn.UseVisualStyleBackColor = true;
            this.ContinueBtn.Click += new System.EventHandler(this.ContinueBtn_Click);
            // 
            // DebugScriptBtn
            // 
            this.DebugScriptBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DebugScriptBtn.Location = new System.Drawing.Point(632, 3);
            this.DebugScriptBtn.Name = "DebugScriptBtn";
            this.DebugScriptBtn.Size = new System.Drawing.Size(79, 23);
            this.DebugScriptBtn.TabIndex = 6;
            this.DebugScriptBtn.Text = "Debug Script";
            this.DebugScriptBtn.UseVisualStyleBackColor = true;
            this.DebugScriptBtn.Click += new System.EventHandler(this.DebugScriptBtn_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 314);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "MainWindow";
            this.Text = "Puppet Master";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox CmdInput;
        private Button RunCmdBtn;
        private TextBox OutputTxtBox;
        private TextBox ScriptPathInput;
        private Button LoadScriptBtn;
        private Button RunScriptBtn;
        private StatusStrip StatusBar;
        private ToolStripStatusLabel StatusLabel;
        private ToolStripProgressBar ProgressBar;
        private ToolStripContainer toolStripContainer1;
        private Button DebugScriptBtn;
        private Button StopBtn;
        private Button ContinueBtn;
    }
}

