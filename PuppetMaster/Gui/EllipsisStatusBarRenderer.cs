﻿using System.Windows.Forms;

namespace PuppetMaster.Gui
{
    public class EllipsisStatusBarRenderer : ToolStripSystemRenderer
    {
        protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e)
        {
            var label = e.Item as ToolStripStatusLabel;

            if (label == null)
            {
                base.OnRenderItemText(e);
                return;
            }

            TextRenderer.DrawText(e.Graphics,
                label.Text,
                label.Font,
                e.TextRectangle,
                label.ForeColor,
                TextFormatFlags.EndEllipsis);
        }
    }
}