﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Windows.Forms;
using NLog;
using PuppetMaster.Business;
using PuppetMaster.Gui;

namespace PuppetMaster
{
    internal static class Program
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            Log.Info("Application started");

            Log.Info("Initializing puppet master service url");
            var startingPort = 20001;
            const int maxPort = 29999;
            var config = new Dictionary<string, string>();
            while (startingPort <= maxPort)
            {
                try
                {
                    config["port"] = startingPort.ToString();
                    config["name"] = "PuppetMaster";
                    var channel = new TcpChannel(
                        config,
                        new BinaryClientFormatterSinkProvider(),
                        new BinaryServerFormatterSinkProvider()
                        );
                    ChannelServices.RegisterChannel(channel, false);
                    break;
                }
                catch (SocketException e)
                {
                    Log.Warn("Error registering channel", e);
                    startingPort += 1;
                }
            }

            var puppetMaster = new PuppetMasterSvcImpl();
            RemotingServices.Marshal(puppetMaster, "PM");
            Log.Debug("Puppet master service started at port {0}", config["port"]);

            var scriptParser = new ScriptParser();

            if (args.Length == 0)
            {
                Log.Info("Starting in GUI mode");
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainWindow(scriptParser));
            }
            else
            {
                Log.Info("Starting console mode");
                try
                {
                    scriptParser.LoadAndExecute(args[0]);
                }
                catch (IOException e)
                {
                    Log.Fatal("Exception loading script, exitting.", e);
                    Console.WriteLine("Unable to read input file");
                }

                Console.WriteLine("Please press Enter to exit");
                Console.ReadLine();
            }
        }
    }
}