﻿% This is the test file
% Line start with % character are ignored

% WORKER <ID> <PUPPETMASTER-URL> <SERVICE-URL> [<ENTRY-URL>|Optional]
WORKER 1 tcp://localhost:20001/PM tcp://localhost:30001/W
WAIT 1
WORKER 2 tcp://localhost:20001/PM tcp://localhost:30002/W tcp://localhost:30001/W
WAIT 1
WORKER 3 tcp://localhost:20001/PM tcp://localhost:30003/W tcp://localhost:30002/W
WAIT 1
WORKER 4 tcp://localhost:20001/PM tcp://localhost:30004/W tcp://localhost:30001/W
WAIT 1
WORKER 5 tcp://localhost:20001/PM tcp://localhost:30005/W tcp://localhost:30002/W

% SLOWW <ID> <delay-in-seconds>
SLOWW 1 15
SLOWW 2 15
SLOWW 3 15
SLOWW 4 15
SLOWW 5 15

% WAIT <SECS>
WAIT 5

% SUBMIT <ENTRY-URL> <FILE> <OUTPUT> <S> <MAP> <DLL>
% SUBMIT tcp://localhost:50001/W TestData/tom_sawyer.txt Output 15 LoggingMapper ../../../TestMapper/bin/Debug/TestMapper.dll
%SUBMIT tcp://localhost:30001/W TestData/tom_sawyer.txt Output-1 15 WordCountMapper ../../../TestMapper/bin/Debug/TestMapper.dll
%SUBMIT tcp://localhost:30005/W TestData/tom_sawyer.txt Output-5 15 WordCountMapper ../../../TestMapper/bin/Debug/TestMapper.dll
%SUBMIT tcp://localhost:30002/W TestData/tom_sawyer.txt Output-2 15 WordCountMapper ../../../TestMapper/bin/Debug/TestMapper.dll
SUBMIT tcp://localhost:30003/W TestData/tom_sawyer.txt Output-3 15 WordCountMapper ../../../TestMapper/bin/Debug/TestMapper.dll
SUBMIT tcp://localhost:30005/W TestData/tom_sawyer.txt Output-4 15 WordCountMapper ../../../TestMapper/bin/Debug/TestMapper.dll

% STATUS
STATUS

WAIT 3

% FREEZEC <ID>
FREEZEC 3
FREEZEC 5
FREEZEC 4

WAIT 30

UNFREEZEC 3

WAIT 30

STATUS

WAIT 30

STATUS