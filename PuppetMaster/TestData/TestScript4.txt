﻿% This is the test file
% Line start with % character are ignored

% WORKER <ID> <PUPPETMASTER-URL> <SERVICE-URL> [<ENTRY-URL>|Optional]
WORKER 1 tcp://localhost:20001/PM tcp://localhost:30001/W
WORKER 2 tcp://localhost:20001/PM tcp://localhost:30002/W tcp://localhost:30001/W
WORKER 3 tcp://localhost:20001/PM tcp://localhost:30003/W tcp://localhost:30002/W
WORKER 4 tcp://localhost:20001/PM tcp://localhost:30004/W tcp://localhost:30002/W

% SLOWW <ID> <delay-in-seconds>
SLOWW 1 10
SLOWW 2 3
SLOWW 3 5
SLOWW 4 30

% WAIT <SECS>
WAIT 10

% SUBMIT <ENTRY-URL> <FILE> <OUTPUT> <S> <MAP> <DLL>
SUBMIT tcp://localhost:30001/W TestData/LargeInput.txt Output 15 WordCountMapper ../../../TestMapper/bin/Debug/TestMapper.dll