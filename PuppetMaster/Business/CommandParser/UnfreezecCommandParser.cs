﻿using System;
using System.Text.RegularExpressions;
using PuppetMaster.Business.CommandExecutor;

namespace PuppetMaster.Business.CommandParser
{
    internal class UnfreezecCommandParser : ICommandParser
    {
        private static readonly Regex Pattern = new Regex(@"^unfreezec (\d+)", RegexOptions.IgnoreCase);

        public ParseResult Parser(string line)
        {
            var match = Pattern.Match(line);
            if (match.Success)
            {
                var id = Convert.ToInt32(match.Groups[1].Value);
                var workerManager = SingleWorkerManagerFactory.GetWorkerManager();
                return new ParseResult(true, new UnfreezecCommandExecutor(id, workerManager));
            }
            return new ParseResult(false, null);
            throw new NotImplementedException();
        }
    }
}