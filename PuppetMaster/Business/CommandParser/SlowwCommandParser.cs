﻿using System;
using System.Text.RegularExpressions;
using PuppetMaster.Business.CommandExecutor;
using PuppetMaster.DataStructure;

namespace PuppetMaster.Business.CommandParser
{
    internal class SlowwCommandParser : ICommandParser
    {
        private static readonly Regex Pattern = new Regex(@"sloww (\d+) (\d+)", RegexOptions.IgnoreCase);

        public ParseResult Parser(string line)
        {
            var match = Pattern.Match(line);
            if (match.Success)
            {
                var id = Convert.ToInt32(match.Groups[1].Value);
                var delay = Convert.ToInt32(match.Groups[2].Value);
                var command = new SlowwCommand(id, delay);
                var workerManager = SingleWorkerManagerFactory.GetWorkerManager();
                var commandExecutor = new SlowwCommandExecutor(command, workerManager);
                return new ParseResult(true, commandExecutor);
            }
            return new ParseResult(false, null);
        }
    }
}