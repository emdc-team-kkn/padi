﻿using PuppetMaster.Business.CommandExecutor;

namespace PuppetMaster.Business.CommandParser
{
    internal class StatusCommandParser : ICommandParser
    {
        public ParseResult Parser(string line)
        {
            if (line.ToLower().Equals("status"))
            {
                return new ParseResult(true, new StatusCommandExecutor());
            }
            return new ParseResult(false, null);
        }
    }
}