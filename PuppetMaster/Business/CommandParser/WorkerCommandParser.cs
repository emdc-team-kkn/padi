﻿using System;
using System.Text.RegularExpressions;
using PuppetMaster.Business.CommandExecutor;
using PuppetMaster.DataStructure;

namespace PuppetMaster.Business.CommandParser
{
    internal class WorkerCommandParser : ICommandParser
    {
        private static readonly Regex Pattern = new Regex(@"worker\s+(\d+)\s+([^\s]+)\s+([^\s]+)(\s+(.+))*",
            RegexOptions.IgnoreCase);

        public ParseResult Parser(string line)
        {
            var match = Pattern.Match(line);
            if (match.Success)
            {
                var id = Convert.ToInt32(match.Groups[1].Value);
                var puppetMasterUrl = match.Groups[2].Value;
                var serviceUrl = match.Groups[3].Value;
                var entryUrl = match.Groups[4].Value.Trim();
                var workerCommand = new WorkerCommand(id, puppetMasterUrl, serviceUrl, entryUrl);
                IPuppetCommandExecutor workerCommandExecutor = new WorkerCommandExecutor(workerCommand);
                return new ParseResult(true, workerCommandExecutor);
            }
            return new ParseResult(false, null);
        }
    }
}