﻿using PuppetMaster.Business.CommandExecutor;

namespace PuppetMaster.Business.CommandParser
{
    internal class ParseResult
    {
        public readonly IPuppetCommandExecutor Executor;
        public readonly bool Success;

        public ParseResult(bool success, IPuppetCommandExecutor executor)
        {
            Success = success;
            Executor = executor;
        }
    }

    internal interface ICommandParser
    {
        ParseResult Parser(string line);
    }
}