﻿using System;
using System.Text.RegularExpressions;
using PuppetMaster.Business.CommandExecutor;
using PuppetMaster.DataStructure;

namespace PuppetMaster.Business.CommandParser
{
    internal class SubmitCommandParser : ICommandParser
    {
        private static readonly Regex Pattern =
            new Regex(@"submit (.+) (.+) (.+) (\d+) (.+) (.+)", RegexOptions.IgnoreCase);

        public ParseResult Parser(string line)
        {
            var match = Pattern.Match(line);
            if (match.Success)
            {
                var entryUrl = match.Groups[1].Value;
                var file = match.Groups[2].Value;
                var output = match.Groups[3].Value;
                var split = Convert.ToInt32(match.Groups[4].Value);
                var map = match.Groups[5].Value;
                var dll = match.Groups[6].Value;
                var command = new SubmitCommand(entryUrl, file, output, split, map, dll);
                var commandExecutor = new SubmitCommandExecutor(command);
                return new ParseResult(true, commandExecutor);
            }
            return new ParseResult(false, null);
        }
    }
}