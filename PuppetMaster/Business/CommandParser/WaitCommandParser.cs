﻿using System;
using System.Text.RegularExpressions;
using PuppetMaster.Business.CommandExecutor;

namespace PuppetMaster.Business.CommandParser
{
    internal class WaitCommandParser : ICommandParser
    {
        private static readonly Regex Pattern = new Regex(@"wait (\d+)", RegexOptions.IgnoreCase);

        public ParseResult Parser(string line)
        {
            var match = Pattern.Match(line);
            if (match.Success)
            {
                var waitTime = Convert.ToInt32(match.Groups[1].Value);
                var commandExecutor = new WaitCommandExecutor(waitTime);
                return new ParseResult(true, commandExecutor);
            }
            return new ParseResult(false, null);
        }
    }
}