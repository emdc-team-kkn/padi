﻿namespace PuppetMaster.Business
{
    internal static class SingleWorkerManagerFactory
    {
        private static WorkerManager _workerManager;

        internal static WorkerManager GetWorkerManager()
        {
            if (_workerManager == null)
            {
                _workerManager = new WorkerManager();
                ;
            }
            return _workerManager;
        }
    }
}