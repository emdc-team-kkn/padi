﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using NLog;
using PuppetMaster.Business.CommandExecutor;
using PuppetMaster.Business.CommandParser;

namespace PuppetMaster.Business
{
    internal delegate void ParseCompleteDelegate(int commandsCount);

    internal delegate void CommandExecutedDelegate(int index);

    public class ScriptParser
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private IPuppetCommandExecutor _currentExecutor;
        private bool noWait;

        private static readonly ICommandParser[] Parsers =
        {
            new WorkerCommandParser(),
            new SubmitCommandParser(),
            new WaitCommandParser(),
            new StatusCommandParser(),
            new SlowwCommandParser(),
            new FreezewCommandParser(),
            new UnfreezewCommandParser(),
            new FreezecCommandParser(),
            new UnfreezecCommandParser()
        };

        private readonly List<IPuppetCommandExecutor> _executors;
        private bool _stop;
        public bool Debug { get; set; }

        internal ScriptParser()
        {
            _executors = new List<IPuppetCommandExecutor>();
            Debug = false;
            _stop = false;
        }

        internal event ParseCompleteDelegate ParseCompleteEvent;
        internal event CommandExecutedDelegate CommandExecutedEvent;



        internal void LoadAndExecute(string filePath)
        {
            _stop = false;
            Load(filePath);
            Execute();
        }

        internal bool ParseAndExecute(string command)
        {
            var executor = ParseLine(command);
            if (executor != null)
            {
                executor.Execute();
                return true;
            }
            return false;
        }

        private void Load(string filePath)
        {
            Log.Info("Parsing file: {0}", filePath);
            using (var reader = new StreamReader(filePath))
            {
                string line;
                var counter = 0;

                while ((line = reader.ReadLine()) != null)
                {
                    counter++;
                    Log.Debug("Processing line {0}: {1}", counter, line);
                    var executor = ParseLine(line);
                    if (executor != null)
                    {
                        _executors.Add(executor);
                    }
                }
            }
            if (ParseCompleteEvent != null)
            {
                ParseCompleteEvent(_executors.Count);
            }
        }

        private void Execute()
        {
            // Run all executor
            var commandCounter = 0;
            try
            {
                foreach (var executor in _executors)
                {
                    if (Debug && !noWait)
                    {
                        lock (this)
                        {
                            Monitor.Wait(this);
                        }
                    }
                    if (_stop)
                    {
                        break;
                    }
                    noWait = false;
                    _currentExecutor = executor;
                    executor.Execute();
                    if (CommandExecutedEvent != null)
                    {
                        CommandExecutedEvent(++commandCounter);
                    }
                    
                }
            }
            catch (Exception e)
            {
                Log.Warn("Exception when running command", e);
            }
            finally
            {
                _executors.Clear();
            }
        }

        public void Continue()
        {
            // Wake up wait command
            if (_currentExecutor is WaitCommandExecutor)
            {
                lock (_currentExecutor)
                {
                    Monitor.PulseAll(_currentExecutor);
                }
                noWait = true;
            }
            lock (this)
            {
                Monitor.PulseAll(this);
            }
        }

        public void Stop()
        {
            _stop = true;
            Continue();
        }

        private static IPuppetCommandExecutor ParseLine(string line)
        {
            line = line.Trim();
            if (line.Length == 0 || line.StartsWith("%"))
            {
                // Ignore line that start with % and empty line
                return null;
            }
            // Check against all the parsers one by one until we found a match
            foreach (var parser in Parsers)
            {
                var parseResult = parser.Parser(line);
                if (parseResult.Success)
                {
                    return parseResult.Executor;
                }
            }
            return null;
        }
    }
}