﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Common;
using NLog;

namespace PuppetMaster.Business
{
    internal class PuppetMasterSvcImpl : MarshalByRefObject, IPuppetMasterSvc
    {
        private const string WorkerPathKey = "WorkerPath";
        private const int WaitTime = 1000;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public bool CreateWorker(int id, string serviceUrl, string entryUrl)
        {
            // Start a local process
            var ret = false;
            var process = new Process();
            try
            {
                var appSetting = ConfigurationManager.AppSettings;

                if (appSetting[WorkerPathKey] == null)
                {
                    throw new ConfigurationErrorsException("Work path not found");
                }
                var executable = appSetting[WorkerPathKey];
                Log.Debug("Creating worker: Id: {0}, ServiceUrl: {1}, EntryUrl: {2}", id, serviceUrl, entryUrl);
                Log.Debug("Worker executable: {0}", Path.GetFullPath(executable));
                if (entryUrl.Length > 0)
                {
                    process.StartInfo = new ProcessStartInfo(Path.GetFullPath(executable),
                        String.Format("\"{0}\" \"{1}\"", serviceUrl, entryUrl));
                }
                else
                {
                    process.StartInfo = new ProcessStartInfo(Path.GetFullPath(executable),
                        String.Format("\"{0}\"", serviceUrl));
                }
                ret = process.Start();
            }
            catch (Exception e)
            {
                Log.Error("Unable to start worker", e);
                ret = false;
            }
            Log.Debug("Wait {0} ms to see if process really started for", WaitTime);
            Thread.Sleep(WaitTime);
            return ret && !process.HasExited;
        }

        /// <summary>
        ///     Override to provide infinite lease time
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}