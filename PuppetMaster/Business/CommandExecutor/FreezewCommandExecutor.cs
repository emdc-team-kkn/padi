﻿using NLog;

namespace PuppetMaster.Business.CommandExecutor
{
    internal class FreezewCommandExecutor : IPuppetCommandExecutor
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly int _id;
        private readonly WorkerManager _workerManager;

        public FreezewCommandExecutor(int id, WorkerManager workerManager)
        {
            _id = id;
            _workerManager = workerManager;
        }

        public void Execute()
        {
            Log.Info("Executing command: FREEZEW {0}", _id);
            _workerManager.FreezeW(_id);
        }
    }
}