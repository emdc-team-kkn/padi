﻿using NLog;

namespace PuppetMaster.Business.CommandExecutor
{
    internal class FreezecCommandExecutor : IPuppetCommandExecutor
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly int _id;
        private readonly WorkerManager _workerManager;

        public FreezecCommandExecutor(int id, WorkerManager workerManager)
        {
            _id = id;
            _workerManager = workerManager;
        }

        public void Execute()
        {
            Log.Info("Executing command: FREEZEC {0}", _id);
            _workerManager.FreezeC(_id);
        }
    }
}