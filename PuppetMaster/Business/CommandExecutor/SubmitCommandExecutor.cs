﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using NLog;
using PuppetMaster.DataStructure;

namespace PuppetMaster.Business.CommandExecutor
{
    internal class SubmitCommandExecutor : IPuppetCommandExecutor
    {
        private const string ClientPathKey = "ClientPath";
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly SubmitCommand _command;

        public SubmitCommandExecutor(SubmitCommand command)
        {
            _command = command;
        }

        public void Execute()
        {
            var appSettings = ConfigurationManager.AppSettings;
            if (appSettings[ClientPathKey] == null)
            {
                throw new ConfigurationErrorsException("Client path not found");
            }
            var executable = appSettings[ClientPathKey];
            var clientPath = Path.GetFullPath(executable);
            var clientArgs = String.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\"",
                Path.GetFullPath(_command.Dll),
                _command.EntryUrl,
                _command.Map,
                Path.GetFullPath(_command.File),
                _command.Split,
                Path.GetFullPath(_command.Output)
                );
            Log.Info("Executing command: {0}", _command);
            Log.Debug("Executing command: {0} {1}", clientPath, clientArgs);
            Process.Start(clientPath, clientArgs);
        }
    }
}