﻿using NLog;

namespace PuppetMaster.Business.CommandExecutor
{
    internal class StatusCommandExecutor : IPuppetCommandExecutor
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public void Execute()
        {
            Log.Info("Executing command: STATUS");
            var workerManager = SingleWorkerManagerFactory.GetWorkerManager();
            workerManager.Status();
        }
    }
}