﻿using System;
using System.Threading;
using NLog;

namespace PuppetMaster.Business.CommandExecutor
{
    internal class WaitCommandExecutor : IPuppetCommandExecutor
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly int _waitTime;

        public WaitCommandExecutor(int waitTime)
        {
            _waitTime = waitTime;
        }

        public void Execute()
        {
            Log.Info("Executing command: WAIT {0}", _waitTime);
            lock (this)
            {
                Monitor.Wait(this, new TimeSpan(0, 0, _waitTime));
            }
        }
    }
}