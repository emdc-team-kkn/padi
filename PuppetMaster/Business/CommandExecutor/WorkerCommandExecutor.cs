﻿using System;
using Common;
using NLog;
using PuppetMaster.DataStructure;

namespace PuppetMaster.Business.CommandExecutor
{
    internal class WorkerCommandExecutor : IPuppetCommandExecutor
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly WorkerCommand _command;

        public WorkerCommandExecutor(WorkerCommand command)
        {
            _command = command;
        }

        public void Execute()
        {
            Log.Info("Executing command: {0}", _command);
            var puppetMasterSvc = (IPuppetMasterSvc) Activator.GetObject(
                typeof (IPuppetMasterSvc), _command.PuppetMasterUrl
                );
            var workerManager = SingleWorkerManagerFactory.GetWorkerManager();
            var started = puppetMasterSvc.CreateWorker(_command.Id, _command.ServiceUrl, _command.EntryUrl);
            if (started)
            {
                workerManager.RegisterWorker(_command.Id, _command.ServiceUrl);
            }
            else
            {
                Log.Debug("Unable to start worker");
            }
        }
    }
}