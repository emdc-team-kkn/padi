﻿using NLog;

namespace PuppetMaster.Business.CommandExecutor
{
    internal class UnfreezewCommandExecutor : IPuppetCommandExecutor
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly int _id;
        private readonly WorkerManager _workerManager;

        public UnfreezewCommandExecutor(int id, WorkerManager workerManager)
        {
            _id = id;
            _workerManager = workerManager;
        }

        public void Execute()
        {
            Log.Info("Executing command: UNFREEZEW id {0}", _id);
            _workerManager.UnfreezeW(_id);
        }
    }
}