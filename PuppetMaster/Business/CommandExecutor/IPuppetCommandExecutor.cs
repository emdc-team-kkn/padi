﻿namespace PuppetMaster.Business.CommandExecutor
{
    internal interface IPuppetCommandExecutor
    {
        void Execute();
    }
}