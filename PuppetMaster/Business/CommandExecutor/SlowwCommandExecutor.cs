﻿using NLog;
using PuppetMaster.DataStructure;

namespace PuppetMaster.Business.CommandExecutor
{
    internal class SlowwCommandExecutor : IPuppetCommandExecutor
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly SlowwCommand _command;
        private readonly WorkerManager _workerManager;

        public SlowwCommandExecutor(SlowwCommand command, WorkerManager workerManager)
        {
            _command = command;
            _workerManager = workerManager;
        }

        public void Execute()
        {
            Log.Info("Executing command: {0}", _command);
            _workerManager.SlowW(_command.Id, _command.Delay);
        }
    }
}