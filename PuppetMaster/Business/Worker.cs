﻿using System;
using Common;
using NLog;

namespace PuppetMaster.Business
{
    public class Worker
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly int _id;
        private readonly string _serviceUrl;

        public Worker(int id, string serviceUrl)
        {
            _id = id;
            _serviceUrl = serviceUrl;
        }

        public void SlowW(int duration)
        {
            Log.Debug("Slowing worker: {0} {1} seconds", _serviceUrl, duration);
            try
            {
                var worker = (IWorkerPuppetSvc) Activator.GetObject(typeof (IWorkerPuppetSvc), _serviceUrl + ".pp");
                worker.SlowWDuration = duration;
            }
            catch (Exception e)
            {
                Log.Warn("Real worker failure, this is not a test", e);
            }
        }

        public void FreezeW()
        {
            Log.Debug("Freezing worker: {0}", _serviceUrl);
            try
            {
                var worker = (IWorkerPuppetSvc) Activator.GetObject(typeof (IWorkerPuppetSvc), _serviceUrl + ".pp");
                worker.FreezeW = true;
            }
            catch (Exception e)
            {
                Log.Warn("Real worker failure, this is not a test", e);
            }
        }

        public void UnfreezeW()
        {
            Log.Debug("Unfreezing worker: {0}", _serviceUrl);
            try
            {
                var worker = (IWorkerPuppetSvc) Activator.GetObject(typeof (IWorkerPuppetSvc), _serviceUrl + ".pp");
                worker.FreezeW = false;
            }
            catch (Exception e)
            {
                Log.Warn("Real worker failure, this is not a test", e);
                ;
            }
        }

        public void FreezeC()
        {
            Log.Debug("Freezing job scheduler for worker: {0}", _serviceUrl);
            try
            {
                var worker = (IWorkerPuppetSvc) Activator.GetObject(typeof (IWorkerPuppetSvc), _serviceUrl + ".pp");
                worker.FreezeC = true;
            }
            catch (Exception e)
            {
                Log.Warn("Real worker failure, this is not a test", e);
            }
        }

        public void UnfreezeC()
        {
            Log.Debug("Unfreezing job scheduler for worker: {0}", _serviceUrl);
            try
            {
                var worker = (IWorkerPuppetSvc) Activator.GetObject(typeof (IWorkerPuppetSvc), _serviceUrl + ".pp");
                worker.FreezeC = false;
            }
            catch (Exception e)
            {
                Log.Warn("Real worker failure, this is not a test", e);
            }
        }

        public void Status()
        {
            Log.Debug("Getting status for worker: {0}", _serviceUrl);
            try
            {
                var worker = (IWorkerPuppetSvc) Activator.GetObject(typeof (IWorkerPuppetSvc), _serviceUrl + ".pp");
                worker.Status();
            }
            catch (Exception e)
            {
                Log.Warn("Unable to get worker status", e);
            }
        }
    }
}