﻿using System.Collections.Generic;
using NLog;

namespace PuppetMaster.Business
{
    public class WorkerManager
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly Dictionary<int, Worker> _workersRepository = new Dictionary<int, Worker>();

        public void RegisterWorker(int id, string serviceUrl)
        {
            lock (this)
            {
                if (_workersRepository.ContainsKey(id))
                {
                    // Duplicate id
                    Log.Warn("Duplicated worker id detected, overwriting");
                }
                // Register the worker at the service url
                var worker = new Worker(id, serviceUrl);
                _workersRepository[id] = worker;
            }
        }

        public void SlowW(int id, int duration)
        {
            var worker = _workersRepository[id];
            worker.SlowW(duration);
        }

        public void FreezeW(int id)
        {
            var worker = _workersRepository[id];
            worker.FreezeW();
        }

        public void UnfreezeW(int id)
        {
            var worker = _workersRepository[id];
            worker.UnfreezeW();
        }

        public void FreezeC(int id)
        {
            var worker = _workersRepository[id];
            worker.FreezeC();
        }

        public void UnfreezeC(int id)
        {
            var worker = _workersRepository[id];
            worker.UnfreezeC();
        }

        public void Status()
        {
            foreach (var worker in _workersRepository.Values)
            {
                worker.Status();
            }
        }
    }
}