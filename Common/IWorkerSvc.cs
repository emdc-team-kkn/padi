﻿namespace Common
{
    public interface IWorkerSvc
    {
        bool SendMapper(byte[] code, string className, string clientUri, long inputSize, int splitCount);
    }
}