﻿namespace Common
{
    public interface IPuppetMasterSvc
    {
        bool CreateWorker(int id, string serviceUrl, string entryUrl);
    }
}