﻿namespace Common
{
    public interface IWorkerPuppetSvc
    {
        bool FreezeW { set; }
        bool FreezeC { set; }
        int SlowWDuration { set; }
        void Status();
    }
}