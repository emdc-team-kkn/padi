﻿using System.Collections.Generic;

namespace Common
{
    public interface IClientSvc
    {
        byte[] ReadInput(long offset, int maxLength, out int readLength);
        void WriteOutput(int splitId, KeyValuePair<string, IList<string>>[] result);
        void ExecutionComplete(bool success);
    }
}