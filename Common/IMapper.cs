﻿using System.Collections.Generic;

namespace Common
{
    public interface IMapper
    {
        IList<KeyValuePair<string, string>> Map(string fileLine);
    }
}